package com.example.application

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class FinishedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finished)

        findViewById<TextView>(R.id.textViewHey).text =
            intent.extras?.getString("NAME")

        findViewById<TextView>(R.id.textViewFrom).text =
            intent.extras?.getString("FROM")

        findViewById<TextView>(R.id.textViewTo).text =
            intent.extras?.getString("TO")

        findViewById<TextView>(R.id.editTextEmailAddress).text =
            intent.extras?.getString("EMAIL")
    }
}