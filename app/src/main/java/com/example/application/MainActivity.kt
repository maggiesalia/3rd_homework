package com.example.application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var editTextName: EditText
    private lateinit var buttonLogin: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("MyData", "onCreate")

        editTextName = findViewById(R.id.editTextName)
        buttonLogin = findViewById(R.id.buttonLogin)

        buttonLogin.setOnClickListener() {

            val user = editTextName.text.toString()

            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("USER", user)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()

        Log.d("MyData", "onStart")
    }

    override fun onResume() {
        super.onResume()

        Log.d("MyData", "onResume")
    }

    override fun onPause() {
        super.onPause()

        Log.d("MyData", "onPause")
    }

    override fun onStop() {
        super.onStop()

        Log.d("MyData", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d("MyData", "onDestroy")
    }
}