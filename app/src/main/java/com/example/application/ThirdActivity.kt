package com.example.application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class ThirdActivity : AppCompatActivity() {

    private lateinit var editTextEmailAddress: EditText
    private lateinit var buttonFinish: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        editTextEmailAddress = findViewById(R.id.editTextEmailAddress)
        buttonFinish = findViewById(R.id.buttonFinish)

        val user: String? = intent.extras?.getString("USER", "Maggie")
        val from: String? = intent.extras?.getString("FROM", "Tusheti")
        val to: String? = intent.extras?.getString("TO", "Mtirala National Park")

        buttonFinish.setOnClickListener() {

            val emailAddress = editTextEmailAddress.text.toString()

            val intent = Intent(this, FinishedActivity::class.java)
            intent.putExtra("USER", user)
            intent.putExtra("FROM", from)
            intent.putExtra("TO", to)
            intent.putExtra("EMAIL", emailAddress)
            startActivity(intent)
        }
    }
}