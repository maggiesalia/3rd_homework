package com.example.application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CalendarView
import android.widget.EditText

class SecondActivity : AppCompatActivity() {

    private lateinit var editTextFrom: EditText
    private lateinit var editTextTo: EditText
    private lateinit var calendarView: CalendarView
    private lateinit var buttonFinishing: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        editTextFrom = findViewById(R.id.editTextFrom)
        editTextTo = findViewById(R.id.editTextTo)
        calendarView = findViewById(R.id.calendarView)
        buttonFinishing = findViewById(R.id.buttonFinishing)

        buttonFinishing.setOnClickListener() {

            val from = editTextFrom.text.toString()
            val to = editTextTo.text.toString()
            val calendarView = calendarView.toString().toInt()

            val intent = Intent(this, ThirdActivity::class.java)
            intent.putExtra("FROM", from)
            intent.putExtra("TO", to)
            intent.putExtra("DATE", calendarView)
            startActivity(intent)
        }
    }
}